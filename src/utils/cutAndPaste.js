const { copyFile, unlink } = require('fs').promises

const cutAndPaste = async (src, dest) => {
        try {
            await copyFile(src, dest)
            unlink(src)
        } catch (e) {
            console.error(e)
        }
}

module.exports = cutAndPaste