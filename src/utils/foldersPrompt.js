const {stat} = require('fs').promises
const prompts = require('prompts')

let isDir = async (path) => {
    try {
        const stats = await stat(path)
        return stats.isDirectory()
    } catch (e) {
        return false
    }
}

const foldersPrompt = async () => {
    const questions = [
        {
            type: 'text',
            name: 'source',
            message: 'The folder where are your files',
            validate: async value => await isDir(value) ? true : `${value} is not a directory`
        },
        {
            type: 'text',
            name: 'dest',
            message: `The folder where you want to organize the files
            (leave blank if you want the same that the source)`,
            validate: async value => await isDir(value) || value == '' ? true : `${value} is not a directory`
        }
    ]
    const data = await prompts(questions)
    const response = {
        SOURCE_DIR: data.source,
        DEST_DIR: data.dest === '' ? data.source : data.dest
    }
    return response
}

module.exports = foldersPrompt;