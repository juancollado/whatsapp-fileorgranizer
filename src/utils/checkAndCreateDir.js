const { mkdirSync } = require('fs')

function checkAndCreateDir(directories, fileDate, actualDir) {
    return new Promise((resolve, reject) => {
        //If the year/month of the file it's not in array of directories
        if ( !directories.includes(fileDate) ) {
            //Create a dir with the name of the year/month and add it to the array
            mkdirSync(`${actualDir}/${fileDate}`)
            //Push the created directory to the arrays of existing directories of years/months
            directories.push(fileDate)
            resolve(`${actualDir.split('/').slice(-1)[0]}/${fileDate}`)
        }
        //If the year/month of the file exists return false
        resolve(false)
    })
}

module.exports = checkAndCreateDir