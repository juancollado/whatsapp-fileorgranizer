const fsPromises = require('fs').promises,
    fs = require('fs'),
    chalk = require('chalk')
    
const checkAndCreateDir = require('./utils/checkAndCreateDir'),
    cutAndPaste = require('./utils/cutAndPaste'),
    foldersPrompt = require('./utils/foldersPrompt')


const Months = {
    '01': 'January',
    '02': 'Febreary',
    '03': 'March',
    '04': 'April',
    '05': 'May',
    '06': 'June',
    '07': 'July',
    '08': 'August',
    '09': 'September',
    '10': 'October',
    '11': 'November',
    '12': 'December'
}

async function start(cb) {    
    try {
        const {SOURCE_DIR, DEST_DIR} = await foldersPrompt()
        console.time('In')

        class File {
            constructor(name) {
                //The original ubication of the file
                this.originalDir = `${SOURCE_DIR}/${name}`
                // Here is used the name of WhatsApp give to the files to determinate the year and the month
                // The 4 firts numbers after IMG- are the year
                this.year = name.split('-')[1].substr(0, 4)
                // The 2 number after the year are the month
                this.month = Months[name.split('-')[1].substr(4, 2)]
                // With the year and the month is determinated wich will the folder to paste the file
                this.dirToPaste = `${DEST_DIR}/${this.year}/${this.month}/${name}`
            }
        }
        
        console.log(chalk.yellow(`\nReading ${chalk.bold(SOURCE_DIR)}\n`))
        
        // --------- READING FOLDERS (for each folder is returning an array of the content inside) --------

        const source = await fsPromises.readdir(SOURCE_DIR)
        //Only read destination folder when is different to source folder
        const dest = DEST_DIR == SOURCE_DIR ? source : await fsPromises.readdir(DEST_DIR)

        //------------------ LISTING FILES IN SOURCE AND DIRECTORIES IN DEST ---------------------

        //Filtering files of folders in source directories
        const files = source.filter((srt) => !fs.statSync(`${SOURCE_DIR}/${srt}`).isDirectory())
        //Get the currents directories in the source folder
        const yearDirectories = dest.filter((str) => fs.statSync(`${DEST_DIR}/${str}`).isDirectory())
        
        console.log(chalk.green(`${chalk.bold(files.length)} Files found.`))

        // ------------------------------ EACH FILE ----------------------------------------

        for (let file in files) {
            const fileData = new File(files[file])

            try {
                //              ------------ Year --------------

                //If a folder with the name of the file's year doesn't exist is created
                const yearDirCheck = await checkAndCreateDir(yearDirectories, fileData.year, DEST_DIR)
                if (yearDirCheck) { console.log(`New directory: ${ chalk.yellow.bold(yearDirCheck) }` )}

                //          ---------------- Month --------------

                //Now the year directory of the current exist
                // Read the year folder for collect month folders inside 
                const monthDirectories = await fsPromises.readdir(`${DEST_DIR}/${fileData.year}`)
                //If a folder with the name of the file's month doesn't exist is created
                const monthDirCheck = await checkAndCreateDir(monthDirectories, fileData.month, `${DEST_DIR}/${fileData.year}`)

                if (monthDirCheck) { console.log(`      ${ chalk.yellow(monthDirCheck)}`) }

                //              ----------- MOVING FILE --------------

                //now that all the folder require are created the file is cut and pasted to the file dest folder
                cutAndPaste(fileData.originalDir, fileData.dirToPaste)
            } catch (e) {
                return console.log(e)
            }
        }
        console.log('After for');
        
        cb(files.length)
    } catch (e) {
        return console.log(e)
    }
}

start((n) => {
    console.log(`\n${chalk.green.bold(n)} files processed`)
    console.timeEnd('In')
})